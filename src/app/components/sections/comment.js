import React, { Component } from 'react';
import { withRouter } from "react-router";
import { List, Avatar } from 'antd';

class Comment extends Component{
  constructor(props){
    super(props);
    this.state={}
  }

  navigateUserId = (id) => {
    if(id === localStorage.getItem('id')){
      this.props.history.push('/myAccount');
    }
    else{
      this.props.history.push('/user/'+id);
    }
  }

  render(){
    return(
      <div>
        <List
          itemLayout="horizontal"
          dataSource={this.props.values}
          style={{paddingLeft:"20px",borderBottom:"1px solid #cacaca"}}
          renderItem={item => (
            <List.Item>
              <List.Item.Meta
                avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                title = <h3 style={{cursor:"pointer"}} onClick={()=>{this.navigateUserId(item.Cid)}}>{item.Cname}</h3>
                description={item.Cmessage}
                />
            </List.Item>
          )}
        />
      </div>
    )
  }
}

export default (withRouter(Comment));
