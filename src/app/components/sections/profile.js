import React,{ Component } from 'react';
import { Divider, Button } from 'antd';

const id = localStorage.getItem('id');

class Profile extends Component{
  constructor(props){
    super(props);
    this.state={
      status:false
    }
  }

  render(){
    return(
      <div className="mb-5">
        <Divider>
          <div style = {{border:"1px solid #cacaca",height:"100px",width:"100px"}}>
            <img height="80px" width="80px" alt="Profile" src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
          </div>
        </Divider>
        <div style={{textAlign:"center"}}>
          <h3>{this.props.values.username}</h3>
          <br />
          <div className = {this.props.values.id === id ? "Hidden" : "Visible"}>
            <Button className="primeButtonFull">Connect with<span className="ml-1">{this.props.values.username}</span></Button>
          </div>
        </div>
      </div>
    )
  }
}

export default Profile;
