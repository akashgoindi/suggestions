import React, { Component } from 'react';

import BriefPost from '../postsFormat/postFormat2';

class Sidebar extends Component{
  constructor(props){
    super(props);
    this.state={}
  }
  render(){
    return(
      <div>
        <div>
          <h3>Most Voted</h3>
            <BriefPost
              values={{
                title:"Galaxy Fold scren issue Hack ?",
                index:"27"
              }}
            />

            <BriefPost
              values={{
                title:"How to think about raising your first venture fund in a beneficial way ?",
                index:"71"
              }}
            />

            <BriefPost
              values={{
                title:"I found an element. Can it be a new element in periodic table?",
                index:"233"
              }}
            />
        </div>
      </div>
    )
  }
}

export default Sidebar;
