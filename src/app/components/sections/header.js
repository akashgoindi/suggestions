import React, { Component } from 'react';
import { withRouter } from "react-router";
import { connect } from 'react-redux';
import {  Menu, Icon, Row, Col, Button, Modal, Input } from 'antd';

import '../../styles/topbar.css';

import { addPost } from '../../actions/post';
import { logout } from '../../actions/auth';

const { TextArea } = Input;

class Header extends Component{
  constructor(props){
    super(props);
    this.state={
      visible:false,
      title:"",
      description:""
    }
  }

    myAccount=()=>{
      this.props.history.push('/myAccount');
    }

    logout=()=>{
      this.props.logout()
    }

    showModal = () => {
      this.setState({
        visible: true,
      });
    }

    handleOk = () =>{
      if(this.state.title !== ""){
        if(this.state.message !== ""){
          this.props.addPost({title:this.state.title, description:this.state.description})
        }else{alert("Can't add a post without Description")}
      }else{alert("Can't add a post without Title")}

      this.setState({
        visible:false,
        title:"",
        description:""
      });
    }

    handleCancel = (e) => {
      console.log(e);
      this.setState({
        visible: false,
      });
    }

    handleChange = (e) => {
      this.setState({
        [e.target.name] : e.target.value
      })
    }

  render(){
    return(
      <div className="topbar">
        <Row>
          <Col xs={12} sm={12} md={12} lg={8} xl={8}>
            <h2 style={{cursor:"pointer", color: "#fff"}} onClick={()=>{this.props.history.push('/')}}>SiteName</h2>
          </Col>

          <Col xs={12} sm={12} md={12} lg={16} xl={16}>

          <Modal
            title="Add Post"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            footer={[
              <Button key="back" onClick={this.handleCancel}>Cancel</Button>,
              <Button key="submit" type="primary" onClick={this.handleOk}>
                Submit
              </Button>,
            ]}
          >
          <div>
            <h3>Title</h3>
              <TextArea onChange = {this.handleChange} value={this.state.title} name="title" rows={2} placeholder="Enter Title"/>
            <h3 className="mt-3">Description</h3>
              <TextArea onChange = {this.handleChange} rows={4} name="description" value={this.state.description} placeholder="Enter Description"/>
         </div>
          </Modal>

          <div style={{textAlign:"right"}}>
          <Menu
                  onClick={this.handleClick}
                  mode="horizontal"
                  overflowedIndicator=<Icon type="bars" />
                >
                  <Menu.Item
                      key="post"
                      onClick={this.showModal}
                    >
                    <Icon type="highlight" />Add Post
                  </Menu.Item>

                  <Menu.Item
                    key="account"
                    onClick={this.myAccount}
                    >
                    <Icon type="user" />My Account
                  </Menu.Item>

                  <Menu.Item
                    key="user"
                    onClick={this.logout}
                    >
                    <Icon type="logout" />Logout, {localStorage.getItem('username')}
                  </Menu.Item>

                </Menu>
          </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default connect(null,{
  addPost,
  logout
 })(withRouter(Header));
