import React,{ Component } from 'react';
import { Icon } from 'antd';

// THIS SKELETON IS USED ON SIDEBAR FOR DISPLAYING POSTS WITH TITLE ONLY

class BriefPost extends Component{
  constructor(props){
    super(props);
    this.state={}
  }
  render(){
    return(
      <div key={this.props.values.index} className="mb-3" style={{padding:"20px",backgroundColor:"#fff"}}>
        <div className="mb-3 postHeader">
              <span>Akash Goindi<span> @23/04/19</span></span>
        </div>
        <div className="postBody">
          <h4>{this.props.values.title}</h4>
        </div>
        <hr />
        <div className="postFooter">
          <Icon type="smile" /> ({this.props.values.index}) Votes
          <Icon className="ml-2" type="message" /> (28) Messages
        </div>
      </div>
    )
  }
}

export default BriefPost;
