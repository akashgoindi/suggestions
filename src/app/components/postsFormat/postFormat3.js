import React,{ Component } from 'react';
import { withRouter } from "react-router";
import { connect } from 'react-redux';
import { Icon, Row, Col, Avatar, Button } from 'antd';
import moment from 'moment';

import { deletePost, likeCounter } from '../../actions/post';

class MyPost extends Component{
  constructor(props){
    super(props);
    this.state={}
  }

  navigatePostId = (id)=>{
      this.props.history.push('/post/'+id);
  }
  navigateUserId = (id) => {
    this.props.history.push('/user/'+id);
  }
  deletePost = (id) => {
    this.props.deletePost(id)
  }

  likeCounter=(id)=>{
    this.props.likeCounter(id)
  }

  render(){
    const upload = this.props.values ;
    return(
      <div key={this.props.values.index} className="mb-3" style={{padding:"20px",backgroundColor:"#fff",boxShadow:"0px 0px 3px #cacaca"}}>
        <div className="mb-2 postHeader">
          <Row>
            <Col xs={4} sm={4} md={2} lg={2} xl={2}>
              <Avatar size="large" src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
            </Col>
            <Col xs={20} sm={20} md={22} lg={22} xl={22}></Col>
              <span onClick={()=>{this.navigateUserId(upload.userId)}}><b>{upload.username}</b></span><br />
              <span>{moment(upload.postedOn).format('DD-MM-YYYY')}</span>
          </Row>
        </div>
        <div className="postBody">
          <h3 onClick={()=>{this.navigatePostId(upload.id)}}>{upload.title}</h3>
          <p>{upload.description}</p>

          <div className="icons-list">
            <Icon type="smile" /> ({upload.likes}) Votes
            <Icon className="ml-2" type="message" /> ({upload.comments}) Comments
          </div>

        </div>
        <hr />
        <div className="postFooter mt-3" style={{textAlign:"center"}}>
          <Button onClick={()=>{this.likeCounter(upload.id)}} size="small" type="primary"><Icon type="caret-up" />Vote</Button>
          <Button size="small" style={{backgroundColor:"#F6A426", color:"#fff",margin:"0px 5px"}}><Icon type="edit" />Edit</Button>
          <Button onClick={()=>this.deletePost(upload.id)} size="small" style={{backgroundColor:"#B70606", color:"#fff"}}><Icon type="delete" />Delete</Button>
        </div>
      </div>
    )
  }
}

export default connect(null,{
   deletePost,
   likeCounter
  })(withRouter(MyPost));
