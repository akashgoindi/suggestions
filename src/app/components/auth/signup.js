import React,{ Component } from 'react';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, notification } from 'antd';
import '../../styles/style.css';
import '../../styles/margins.css';

import { newUser, authReset } from '../../actions/auth.js';

class Signup extends Component{
  constructor(props){
    super(props);
    this.state = {
      username:"",
      email:"",
      password:""
    }
  }

  componentWillReceiveProps=(nextProps)=>{
  if(nextProps.auth.signupMessage){
    if(nextProps.auth.signup){
        notification['success']({
          message: "Signup Success",
          description: `Welcome to the Family... Please Login Manually`
        });
    }
    else{
        notification['error']({
          message: "Signup Fail",
          description: "Email already exists.... Try a diferent one."
        });
    }
    this.props.authReset();
  }
}

  handleSubmit = (e) => {
   e.preventDefault();
   this.props.form.validateFields((err, values) => {
     if (!err) {
       if(values.repassword === values.password){
         this.props.newUser({
           email:values.email,
           password:values.password,
           username:values.username
         })
       }
       else{alert("Password doesn't match")}
     }
   });
 }

  render(){
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <div className="mb-5">
          <h2>Signup to our site</h2>
          <p>Be a part of us by just providing following details.</p>
        </div>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [{ required: true, message: 'Please input your email!' }],
            })(
              <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input your Password!' }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('repassword', {
              rules: [{ required: true, message: 'Please Re-Enter your Password!' }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="RepeatPassword" />
            )}
          </Form.Item>
          <Form.Item>
            <Button type="default"  htmlType="submit" className="login-form-button primeButtonFull">
              Create Account
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = ({auth}) => {
    return({
      auth
    })
}

const WrappedSignupForm = Form.create({ name: 'normal_login' })(Signup);
export default connect(mapStateToProps,{
  newUser,
  authReset
})(WrappedSignupForm);
