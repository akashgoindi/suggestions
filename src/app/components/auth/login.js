import React,{ Component } from 'react';
import { Redirect, withRouter } from 'react-router'
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, notification } from 'antd';
import '../../styles/style.css';
import '../../styles/margins.css';

import { userLogin, authReset } from '../../actions/auth';

class Login extends Component{
  constructor(props){
    super(props);
    this.state = {}
  }

  componentWillReceiveProps=(nextProps)=>{
  if(nextProps.auth.message){
    if(nextProps.auth.loggedin){
        notification['success']({
          message: "Login Success",
          description: `Good to connect to you, ${localStorage.getItem('username')}`
        });
      this.props.history.push("/");
    }
    else{
        notification['error']({
          message: "Login Fail",
          description: "Please Enter Valid Credentials"
        });
    }
    this.props.authReset();
  }
}

  handleSubmit = (e) => {
   e.preventDefault();
   this.props.form.validateFields((err, values) => {
     if (!err) {
       this.props.userLogin({
         email:values.email,
         password:values.password
       })
     }
   });
 }

  render(){
    if(localStorage.getItem('token')){
      return <Redirect to = '/' />
    }
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <div className="mb-5">
          <h2>Login to continue</h2>
          <p>Users are waiting for you suggestions</p>
        </div>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [{ required: true, message: 'Please input your Email!' }],
            })(
              <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input your Password!'}],
            })(
              <Input autoComplete="new-password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
            )}
          </Form.Item>
          <Form.Item>
          <div className="centerTextDiv">
            <Button  htmlType="submit" className="login-form-button primeButtonFull">Log in</Button>
          </div>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = ({auth}) => {
    return({
      auth
    })
}

const WrappedLoginForm = Form.create({ name: 'normal_login' })(Login);
export default connect(mapStateToProps,
  { userLogin,
    authReset
  })(withRouter(WrappedLoginForm));
