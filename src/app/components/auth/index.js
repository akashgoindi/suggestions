import React, { Component } from 'react';
import { Tabs, Icon, Row, Col } from 'antd';

import WrappedLoginForm from './login';
import WrappedSignupForm from './signup';

const TabPane = Tabs.TabPane;

class AuthenticationPage extends Component{
  constructor(props){
    super(props);
    this.state={}
  }
  render(){
    return(
      <div>
        <Row style={{minHeight:"100vh", backgroundColor:"#326667"}}>
          <Col xs={0} sm={12} md={12} lg={16} xl={16}></Col>
          <Col xs={24} sm={12}  md={12} lg={8} xl={8}>
          <div style={{textAlign:"center",backgroundColor:"#fff",height:"100vh",padding:"20px"}}>
            <Tabs defaultActiveKey="1">
              <TabPane tab={<span><Icon type="user" />Login</span>} key="1">
                <WrappedLoginForm />
              </TabPane>
              <TabPane tab={<span><Icon type="user" />Signup</span>} key="2">
                <WrappedSignupForm />
              </TabPane>
            </Tabs>
          </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default AuthenticationPage ;

// <div style={{textAlign:"center"}}>
//   <Tabs defaultActiveKey="1">
//     <TabPane tab={<span><Icon type="user" />Login</span>} key="1">
//       <Login />
//     </TabPane>
//     <TabPane tab={<span><Icon type="user" />Signup</span>} key="2">
//       <Signup />
//     </TabPane>
//   </Tabs>
// </div>
