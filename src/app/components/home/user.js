import React,{ Component } from 'react';
import { Redirect } from 'react-router';
import {connect} from 'react-redux';
import { Layout, Row, Col, Empty, Tabs, Card } from 'antd';

import Topbar from '../sections/header';
import FooterBlock from '../sections/footer';
import Post from '../postsFormat/postFormat1';
import Profile from '../sections/profile';

import { getUserPosts } from '../../actions/post';

const { Header, Content, Footer } = Layout;
const TabPane = Tabs.TabPane;

class User extends Component{
  constructor(props){
    super(props);
    this.state={
      userPosts:[],
      userDetails:{}
    }
  }

  componentDidMount=()=>{
    this.props.getUserPosts(this.props.match.params.id)
  }

  componentWillReceiveProps=(nextProps)=>{
    this.setState({
      userPosts : nextProps.posts,
      userDetails : nextProps.userDetails
    })
  }

  renderContent=()=>{
    return(
      <div>
        <Row>
          <Col xs={24} sm={16} md={16} lg={18} xl={18} style={{padding:"20px 30px"}}>
          <Profile values={{
            email:this.state.userDetails.email,
            username:this.state.userDetails.username,
            id:this.state.userDetails.id
          }}/>
          <Tabs defaultActiveKey="1" onChange={this.callback}>
            <TabPane tab="Posts" key="1">
            {
                this.state.userPosts && this.state.userPosts.length ?
                  this.state.userPosts.map((post,index)=>{
                    return(
                      <Post
                        key={index}
                        values={{
                          index : index,
                          id:post._id,
                          title : post.title,
                          username:post.username,
                          userId:post.user,
                          description : post.description,
                          postedOn: post.postedOn,
                          likes:post.likes.length,
                          comments:post.comments.length
                        }}
                      />
                    )
                  })
                :
                <div><Empty description="No Posts Available at the moment" /></div>
            }

            </TabPane>

            <TabPane tab="About" key = "2">Content of Tab Pane 3</TabPane>
            </Tabs>

          </Col>

          <Col xs={24} sm={8} md={8} lg={6} xl={6} style={{minHeight:"100vh",backgroundColor:"#eee"}}>

          <Card
             hoverable
             cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
            >
             <Card.Meta
               title="Europe Street beat"
               description="www.instagram.com"
             />
            </Card>
            <br />
            <Card
               hoverable
               cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
              >
               <Card.Meta
                 title="Europe Street beat"
                 description="www.instagram.com"
               />
              </Card>

          </Col>
        </Row>
      </div>
    )
  }

  render(){
    if(!localStorage.getItem('token')){
      return <Redirect to = '/auth' />
    }
    return(
      <div>
        <Layout>
          <Header style={{backgroundColor:"#326667"}}><Topbar /></Header>
          <Layout>
            <Content style={{padding:"15px", minHeight:"100vh", backgroundColor:"#f5f5f5"}}>{this.renderContent()}</Content>
          </Layout>
            <Footer style={{backgroundColor:"#cacaca"}}><FooterBlock /></Footer>
        </Layout>
      </div>
    )
  }
}

const mapStateToProps=({post, auth})=>{
  return({
    posts : post.userPost,
    userDetails:post.userDetails
  })
}

export default connect(mapStateToProps,{getUserPosts})(User);
