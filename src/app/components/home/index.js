import React,{ Component } from 'react';
import { Redirect } from 'react-router';
import { Layout, Row, Col, Empty, Skeleton  } from 'antd';
import { connect } from 'react-redux';

import Post from '../postsFormat/postFormat1';
import Topbar from '../sections/header';
import Sidebar from '../sections/sidebar';
import FooterBlock from '../sections/footer';

import { getAllPosts } from '../../actions/post';

const { Header, Content, Footer } = Layout;


class Home extends Component{
  constructor(props){
    super(props);
    this.state = {
      allPosts:this.props.allposts,
      loading:true
    }
  }

  componentDidMount=()=>{
    this.props.getAllPosts();
  }

  componentWillReceiveProps=(nextProps)=>{
    this.setState({
      allPosts:nextProps.allposts,
      loading:nextProps.loading
    })
  }

  renderContent = () => {
    return(
      <div>
        <Row>
          <Col xs={24} sm={14} md={16} lg={16} xl={16}>
            <div className="containerDiv">
            <div>
              <h3>Latest Posts</h3>
            </div>
            {
              this.state.loading === true ?
              <div>
                <Skeleton active />
                <Skeleton active />
                <Skeleton active />
                <Skeleton active />
              </div>
              :
                this.state.allPosts && this.state.allPosts.length ?
                  this.state.allPosts.map((post,index)=>{
                    return(
                      <Post
                        key={index}
                        values={{
                          index : index,
                          id:post._id,
                          title : post.title,
                          username:post.username,
                          userId:post.user,
                          description : post.description,
                          postedOn: post.postedOn,
                          likes:post.likes.length,
                          comments:post.comments.length
                        }}
                      />
                    )
                  })
                :
                <div><Empty description="No Posts Available at the moment" /></div>
            }

            </div>
          </Col>

          <Col xs={24} sm={10} md={8} lg={8} xl={8}><Sidebar /></Col>
        </Row>
      </div>
    )
  }

  render(){
    if(!localStorage.getItem('token')){
      return <Redirect to = '/auth' />
    }
    return(
      <div>
        <Layout>
          <Header style={{backgroundColor:"#326667"}}><Topbar /></Header>
          <Layout>
            <Content style={{padding:"15px", minHeight:"100vh", backgroundColor:"#f5f5f5"}}>{this.renderContent()}</Content>
          </Layout>
            <Footer style={{backgroundColor:"#cacaca"}}><FooterBlock /></Footer>
        </Layout>
      </div>
    )
  }
}

const mapStateToProps=({post})=>{
  return({
    allposts:post.allposts,
    loading:post.loading
  })
}

export default connect(mapStateToProps,{getAllPosts})(Home);
