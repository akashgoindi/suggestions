import React,{ Component } from 'react';
import { Redirect } from 'react-router';
import {connect} from 'react-redux';
import moment from 'moment';
import { Layout, Icon, Input, Row, Col, Avatar, Button, Empty, Card  } from 'antd';

import Topbar from '../sections/header';
import FooterBlock from '../sections/footer';
import Comment from '../sections/comment';

import { getPost, addComment, likeCounter } from '../../actions/post';

const { Header, Content, Footer } = Layout;
const {TextArea} = Input;

class OpenPost extends Component{
  constructor(props){
    super(props);
    this.state={
      singlePost:{},
      loading:true,
      makeComment:false,
      message:""
    }
  }

  componentDidMount=()=>{
    this.props.getPost(this.props.match.params.id)
  }

  componentWillReceiveProps=(nextProps)=>{
    this.setState({
      singlePost : nextProps.post,
      loading:false,
      makeComment: false
    })
  }

  likeCounter=(id)=>{
    this.props.likeCounter(id)
  }

  setVisibility=()=>{
    this.setState({
      makeComment:!this.state.makeComment
    })
  }

  handleChange = ((e)=>{
    this.setState({
      message:e.target.value
    })
  })

  submitComment=(id)=>{
    if(this.state.message !== ""){
        this.props.addComment({message:this.state.message},id)
    }else{
      alert("Can't send blank comment")
    }
    this.setState({
      message:""
    })
  }

  renderContent=()=>{
    return(
      <div>
        <Row>
          <Col xs={24} sm={16} md={16} lg={18} xl={18} style={{padding:"20px 30px"}}>
          {
            this.state.singlePost  ?
            <div>
              <div>
                <Row>
                  <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                     <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" size="large" />
                     <span className="ml-3"><b>{this.state.singlePost.username}</b></span>

                  </Col>
                  <Col xs={12} sm={12} md={12} lg={12} xl={12} style={{textAlign:"right"}}>
                    <Button onClick={()=>this.likeCounter(this.state.singlePost._id)} className="primeButton"><Icon type="caret-up" />Vote</Button>
                    <Button onClick={this.setVisibility} className="ml-2" type="primary">Comment</Button>
                  </Col>
                </Row>
              </div>

                <div className="mt-3" style={{ textAlign:"justify", padding:"20px",border:"1px solid #cacaca" }}>
                  <h2>{this.state.singlePost.title}</h2><span>posted on <span>{moment(this.state.singlePost.postedOn).format('DD-MM-YYYY')}</span></span>
                  <hr />
                  <p>{this.state.singlePost.description}</p>
                 <div className="icons-list">
                    <Icon type="smile" /> ({this.state.singlePost.likes && (this.state.singlePost.likes.length)}) Votes
                    <Icon className="ml-2" type="message" /> ({this.state.singlePost.comments && (this.state.singlePost.comments.length)}) Comments
                  </div>
                </div>
                <div style={{margin:"20px 0px",textAlign:"center"}} className = {this.state.makeComment ? "Visible" : "Hidden"}>
                  <TextArea onChange={this.handleChange} value={this.state.message} className="mb-3" rows={2} placeholder="Write a Comment..." />
                  <Button onClick={()=>this.submitComment(this.state.singlePost._id)} className="primeButton">Comment</Button>
                </div>

                <h3 style={{margin:"10px 0px"}}>Comments</h3>

                <div style={{backgroundColor:"#eee", maxHeight:"300px",overflow:"scroll",overflowX:"hidden"}}>
                  {
                    this.state.singlePost.comments && this.state.singlePost.comments.length  ?
                      this.state.singlePost.comments.map((comment,index)=>{
                        return(
                          <Comment key={index} values={[{
                            Cid:comment.userid,
                            Cname:comment.username,
                            Cmessage:comment.message
                          }]} />
                        )
                      })
                      :
                      <div style={{padding:"20px",textAlign:"center"}}>
                        <h3>No Comments Yet</h3>
                        <p>Be the first one to comment.</p>
                      </div>
                  }
                </div>
              </div>
              :
              <div><Empty /></div>
          }
          </Col>

          <Col xs={0} sm={8} md={8} lg={6} xl={6} style={{minHeight:"100vh",backgroundColor:"#eee"}}>

          <Card
             hoverable
             cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
            >
             <Card.Meta
               title="Europe Street beat"
               description="www.instagram.com"
             />
            </Card>
            <br />
            <Card
               hoverable
               cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
              >
               <Card.Meta
                 title="Europe Street beat"
                 description="www.instagram.com"
               />
              </Card>

          </Col>
        </Row>
      </div>
    )
  }

  render(){
    if(!localStorage.getItem('token')){
      return <Redirect to = '/auth' />
    }
    return(
      <div>
        <Layout>
          <Header style={{backgroundColor:"#326667"}}><Topbar /></Header>
          <Layout>
            <Content style={{padding:"15px", minHeight:"100vh", backgroundColor:"#f5f5f5"}}>{this.renderContent()}</Content>
          </Layout>
            <Footer style={{backgroundColor:"#cacaca"}}><FooterBlock /></Footer>
        </Layout>
      </div>
    )
  }
}

const mapStateToProps=({post})=>{
  return({
    post : post.singlePost
  })
}

export default connect(mapStateToProps,{
  getPost,
  addComment,
  likeCounter
})(OpenPost);
