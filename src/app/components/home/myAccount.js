import React,{ Component } from 'react';
import { Redirect } from 'react-router';
import {connect} from 'react-redux';
import { Layout, Row, Col, Empty, Tabs, Card } from 'antd';

import Topbar from '../sections/header';
import FooterBlock from '../sections/footer';
import MyPost from '../postsFormat/postFormat3';
import Profile from '../sections/profile';

import { getPost, getAccountDetails } from '../../actions/post';

const { Header, Content, Footer } = Layout;
const TabPane = Tabs.TabPane;

class MyAccount extends Component{
  constructor(props){
    super(props);
    this.state={
      myAllPosts:this.props.posts,
      userDetails:{}
    }
  }

  componentDidMount=()=>{
    this.props.getAccountDetails();
  }

  componentWillReceiveProps=(nextProps)=>{
    this.setState({
      myAllPosts : nextProps.posts,
      userDetails : nextProps.userDetails
    })
  }

  callback=(key)=>{
    // console.log(key);
  }

  renderContent=()=>{
    return(
      <div>
        <Row>
          <Col xs={24} sm={16} md={16} lg={18} xl={18} style={{padding:"20px 30px"}}>

          <Profile values={{
            email:this.state.userDetails.email,
            username:this.state.userDetails.username,
            id:this.state.userDetails.id
          }}/>

            <Tabs defaultActiveKey="1" onChange={this.callback}>
              <TabPane tab="All Posts" key="1">
                {
                  this.state.myAllPosts && this.state.myAllPosts.length ?
                    this.state.myAllPosts.map((post,index)=>{
                      return(
                        <MyPost
                          key={index}
                          values={{
                            index : index,
                            id:post._id,
                            title : post.title,
                            username:post.username,
                            userId:post.user,
                            description : post.description,
                            postedOn: post.postedOn,
                            likes:post.likes.length,
                            comments:post.comments.length
                          }}
                        />
                      )
                    })
                  :
                  <Empty />
                }

              </TabPane>
              <TabPane tab="Most Liked" key="2">
                2
              </TabPane>
              <TabPane tab="Connections" key="3">
                3
              </TabPane>
              <TabPane tab="About" key="4">
                4
              </TabPane>
            </Tabs>

          </Col>

          <Col xs={0} sm={8} md={8} lg={6} xl={6} style={{minHeight:"100vh",backgroundColor:"#eee"}}>

          <Card
             hoverable
             cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
            >
             <Card.Meta
               title="Europe Street beat"
               description="www.instagram.com"
             />
            </Card>
            <br />
            <Card
               hoverable
               cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
              >
               <Card.Meta
                 title="Europe Street beat"
                 description="www.instagram.com"
               />
              </Card>


          </Col>
        </Row>
      </div>
    )
  }

  render(){
    if(!localStorage.getItem('token')){
      return <Redirect to = '/auth' />
    }
    return(
      <div>
        <Layout>
          <Header style={{backgroundColor:"#326667"}}><Topbar /></Header>
          <Layout>
            <Content style={{padding:"15px", minHeight:"100vh", backgroundColor:"#f5f5f5"}}>{this.renderContent()}</Content>
          </Layout>
            <Footer style={{backgroundColor:"#cacaca"}}><FooterBlock /></Footer>
        </Layout>
      </div>
    )
  }
}

const mapStateToProps=({post})=>{
  return({
    posts : post.myAllPosts,
    userDetails:post.userDetails
  })
}

export default connect(mapStateToProps,{getPost, getAccountDetails})(MyAccount);
