import React from 'react';
import 'antd/dist/antd.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import reducer from './reducers'
import Routers from './routers';

function App() {
  const store = createStore(reducer,applyMiddleware(thunk));
  return (
    <Provider store={store}>
      <Routers />
    </Provider>
  );
}

export default App;
