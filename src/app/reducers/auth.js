import {
  CREATE_NEW_USER,
  LOGIN_USER,
  AUTH_RESET
} from '../actionTypes'

const initState = {
  loggedin:false,
  message:"",
  signupMessage:"",
  signup:false
}

const authReducer = ( state=initState, action) => {
  switch(action.type){
/////////////////////////////////////////
    case CREATE_NEW_USER :
    if(action.payload.errorMessage){
    console.log(action.payload.message)
    }
    else{
      state = {...state,...action.payload}
    }
    return state;
/////////////////////////////////////////
    case LOGIN_USER:
    if(action.payload.errorMessage){
      console.log(action.payload.message)
    }
    else{
      state = {...state,...action.payload}
    }
    return state;
/////////////////////////////////////////
    case AUTH_RESET :
    state={...initState}
    return state;
////////////////////////////////////////////////
    default:
      return state;
  }
}
export default authReducer;
