import {
  GET_ALL_POSTS,
  GET_POST,
  GET_USER_POSTS,
  GET_ACCOUNT,
  LIKE_POST,
  COMMENT_POST,
  ADD_POST,
  DELETE_POST
} from '../actionTypes';

const initState = {
  allposts:[],
  loading:true,
  singlePost:{},
  userPost:[],
  myAllPosts:[],
  userDetails:{}
}

const postReducer = ( state=initState, action) => {

  switch(action.type){
////////////////////////////////////////
    case GET_ALL_POSTS :
    if(action.payload.errorMessage){
      console.log(action.payload.errorMessage)
    }
    else{
        state={
          ...state,
          allposts:action.payload,
          loading:false
        }
    }
    return state;
////////////////////////////////////////
    case GET_POST :
    if(action.payload.errorMessage){
      console.log(action.payload.errorMessage)
    }
    else{
        state={
          ...state,
          singlePost:action.payload.post
        }
    }
    return state;
/////////////////////////////////////

case GET_USER_POSTS :
if(action.payload.errorMessage){
  console.log(action.payload.errorMessage)
}
else{
    state={
      ...state,
      userPost:[...action.payload.posts],
      userDetails:action.payload.userDetails
    }
}
return state;

////////////////////////////////////////

  case GET_ACCOUNT:
    if(action.payload.errorMessage){
      console.log(action.payload.errorMessage)
    }
    else{
      state={
        ...state,
        myAllPosts:[...action.payload.posts],
        userDetails:action.payload.userDetails
      }
    }
    return state;

//////////////////////////////////////
  case LIKE_POST:
    if(action.payload.errorMessage){
      console.log(action.payload.errorMessage)
    }
    else{
      var test1 = state.allposts;
      var test2 = state.userPost;
      var test3 = state.myAllPosts;

      const temp1 = test1.filter((post) => {
        return(
          post._id === action.payload._id
        )
    })
    const temp2 = test2.filter((post) => {
      return(
        post._id === action.payload._id
      )
  })
  const temp3 = test3.filter((post) => {
    return(
      post._id === action.payload._id
    )
})

    if(temp1.length){
      temp1[0].likes = [...action.payload.likes]
    }
    if(temp2.length){
      temp2[0].likes = [...action.payload.likes]
    }
    if(temp3.length){
      temp3[0].likes = [...action.payload.likes]
    }

    state={
      ...state,
      allposts:[...test1],
      userPost:[...test2],
      myAllPosts:[...test3],
      singlePost:action.payload

    }
  }
    return state;
/////////////////////////////////////
case COMMENT_POST :
if(action.payload.errorMessage){
  console.log(action.payload.errorMessage)
}
else{
    state={
      ...state,
      singlePost:action.payload
    }
}
return state;
/////////////////////////////////////
case ADD_POST :
if(action.payload.errorMessage){
  console.log(action.payload.errorMessage)
}
else{
  console.log(action.payload)
    state={
      ...state,
      allposts:[action.payload.post,...state.allposts],
      myAllPosts:[action.payload.post,...state.myAllPosts]
    }
}
return state;
////////////////////////////////////////
case DELETE_POST :
if(action.payload.errorMessage){
  console.log(action.payload.errorMessage)
}
  else{
    const temp = state.myAllPosts.filter((post) => {
      return(
        post._id !== action.payload._id
      )
  })
  state={
    ...state,
    myAllPosts:temp
  }
}

return state;
////////////////////////////////////////
    default:
      return state
  }
}

export default postReducer;
