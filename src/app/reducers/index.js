import { combineReducers } from 'redux';

import postReducer from './post.js';
import authReducer from './auth.js';

const reducer = combineReducers({
  auth: authReducer,
  post: postReducer
})

export default reducer;
