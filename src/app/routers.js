import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './components/home';
import OpenPost from './components/home/openPost';
import User from './components/home/user';
import MyAccount from './components/home/myAccount';
import AuthenticationPage from './components/auth';

const Routers = () => {
  return(
  <BrowserRouter>
    <Switch>
      <Route exact path = "/" component = { Home } />
      <Route path = "/auth" component = { AuthenticationPage } />
      <Route path = "/post/:id" component = { OpenPost } />
      <Route path = "/user/:id" component = { User } />
      <Route path = "/myAccount" component = { MyAccount } />
    </Switch>
  </BrowserRouter>
)
}

export default Routers;
