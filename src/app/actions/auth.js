import {
  CREATE_NEW_USER,
  LOGIN_USER,
  AUTH_RESET
} from '../actionTypes.js';

import { newUserCall, userLoginCall } from './apicalls/auth';

export const newUser = (json_data) => {
  return((dispatch)=>{
    newUserCall(dispatch,{actionType:CREATE_NEW_USER, ...json_data});
  })
}

export const userLogin = (json_data) => {
  return((dispatch)=>{
    userLoginCall(dispatch,{actionType:LOGIN_USER, ...json_data});
  })
}

export const authReset = () => {
    return({
      type:AUTH_RESET
    })
}

export const logout = () => {
  localStorage.removeItem('id');
  localStorage.removeItem('token');
  localStorage.removeItem('username');
  window.location = '/';
}
