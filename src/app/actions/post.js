import {
  GET_ALL_POSTS,
  GET_POST,
  GET_USER_POSTS,
  GET_ACCOUNT,
  LIKE_POST,
  COMMENT_POST,
  ADD_POST,
  DELETE_POST
} from '../actionTypes.js';

import { getAllPostsCall,
   getPostCall,
   getAccountDetailsCall,
   getUserPostsCall,
   likeCounterCall,
   addCommentCall,
   addPostCall,
   deletePostCall} from './apicalls/post';

export const getAllPosts = () => {
  return((dispatch)=>{
    getAllPostsCall(dispatch,GET_ALL_POSTS);
  })
}

export const getPost = (id) => {
  return((dispatch)=>{
    getPostCall(dispatch,GET_POST,id)
  })
}

export const getAccountDetails = () => {
  return((dispatch)=>{
    getAccountDetailsCall(dispatch,GET_ACCOUNT)
  })
}

export const getUserPosts = (id) => {
  return((dispatch)=>{
    getUserPostsCall(dispatch,GET_USER_POSTS,id)
  })
}

export const likeCounter = (id) => {
  return((dispatch)=>{
    likeCounterCall(dispatch,LIKE_POST,id)
  })
}

export const addComment = (jsonData,id) => {
  return((dispatch)=>{
    addCommentCall(dispatch,COMMENT_POST,jsonData,id)
  })
}

export const addPost = (jsonData) => {
  return((dispatch)=>{
    addPostCall(dispatch,ADD_POST,jsonData)
  })
}

export const deletePost = (id) => {
  return((dispatch)=>{
    deletePostCall(dispatch,DELETE_POST,id)
  })
}
