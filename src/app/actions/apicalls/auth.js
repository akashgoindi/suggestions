import axios from 'axios';

export async function newUserCall(dispatch,receivedData){
  var {actionType,...data} = receivedData;
  axios.post("https://suggestions123.herokuapp.com/user/signup",data)
  .then((response)=>{
    if(response.status === 200){
      dispatch({
        type:actionType,
        payload: {signup:true,signupMessage:"Signup Successful"}
      })
    }
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload: {signup:false,signupMessage:"Please enter valid credentials"}
    })
  })
}


export async function userLoginCall(dispatch,receivedData){
  var {actionType,...data} = receivedData;
  axios.post("https://suggestions123.herokuapp.com/user/login",data)
  .then((response)=>{
    if(response.status === 200){
      localStorage.setItem('token',response.data.token)
      localStorage.setItem('id',response.data._id)
      localStorage.setItem('username',response.data.username)
      dispatch({
        type:actionType,
        payload:{loggedin:true,message:"Login Successful"}
      })
    }
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload: {loggedin:false,message:"Please enter valid credentials"}
    })
  })
}
