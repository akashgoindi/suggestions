import axios from 'axios';

export async function getAllPostsCall(dispatch,actionType){
  axios.get("https://suggestions123.herokuapp.com/")
  .then((response)=>{
    dispatch({
      type:actionType,
      payload:[...response.data]
    })
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload:{errorMessage:error}
    })
  })
}

export async function getPostCall(dispatch,actionType,id){
  axios.get("https://suggestions123.herokuapp.com/"+id)
  .then((response)=>{
    dispatch({
      type:actionType,
      payload:{...response.data}
    })
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload:{errorMessage:error}
    })
  })
}

export async function getUserPostsCall(dispatch,actionType,id){
  axios.get("https://suggestions123.herokuapp.com/user/"+id)
  .then((response)=>{
    dispatch({
      type:actionType,
      payload:{...response.data}
    })
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload:{errorMessage:error}
    })
  })
}

export async function getAccountDetailsCall(dispatch,actionType){
  var token = localStorage.getItem('token');
  axios.post('https://suggestions123.herokuapp.com/myAccount',{},{headers:{
    Authorization:`Bearer ${token}`
  }})
  .then((response)=>{
    dispatch({
      type:actionType,
      payload:{...response.data}
    })
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload:{errorMessage:error}
    })
  })
}

export async function likeCounterCall(dispatch,actionType,id){
  var token = localStorage.getItem('token');
  axios.put('https://suggestions123.herokuapp.com/post/update/'+id,{},{headers:{
    Authorization:`Bearer ${token}`
  }})
  .then((response)=>{
    dispatch({
      type:actionType,
      payload:{...response.data}
    })
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload:{errorMessage:error}
    })
  })
}

export async function addCommentCall(dispatch,actionType,jsonData,id){
  var token = localStorage.getItem('token');
  axios.put("https://suggestions123.herokuapp.com/post/comment/"+id,jsonData,{
    headers:{
    Authorization:`Bearer ${token}`
  }}
)
  .then((response)=>{
    dispatch({
      type:actionType,
      payload:{...response.data}
    })
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload:{errorMessage:error}
    })
  })
}

export async function addPostCall(dispatch,actionType,jsonData){
  var token = localStorage.getItem('token');
  axios.post("https://suggestions123.herokuapp.com/post/add_post/",jsonData,{
    headers:{
    Authorization:`Bearer ${token}`
  }})
  .then((response)=>{
    dispatch({
      type:actionType,
      payload:{...response.data}
    })
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload:{errorMessage:error}
    })
  })
}

export async function deletePostCall(dispatch,actionType,id){
  var token = localStorage.getItem('token');
  axios.delete("https://suggestions123.herokuapp.com/post/remove/"+id,{
    headers:{
    Authorization:`Bearer ${token}`
  }})
  .then((response)=>{
    dispatch({
      type:actionType,
      payload:{...response.data}
    })
  })
  .catch((error)=>{
    dispatch({
      type:actionType,
      payload:{errorMessage:error}
    })
  })
}
