//posts
export const GET_ALL_POSTS = "GET_ALL_POSTS";
export const GET_POST = "GET_POST";
export const GET_USER_POSTS = "GET_USER_POSTS";
export const LIKE_POST = "LIKE_POST";
export const COMMENT_POST = "COMMENT_POST";
export const ADD_POST = "ADD_POST";
export const DELETE_POST = "DELETE_POST";

//authentication
export const CREATE_NEW_USER = "CREATE_NEW_USER";
export const LOGIN_USER = "LOGIN_USER";
export const AUTH_RESET = "AUTH_RESET";
export const GET_ACCOUNT = "GET_ACCOUNT";
export const USER_CONNECT = "USER_CONNECT";
